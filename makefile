# Make draft run control program for local DAQ stress rests
#
# PG v.0.3 18 June 2018
#-------------------------------------------------------
project=rc

#----------------------------------------------------- check shell variables
define wrn_
$(info >> Warning: undefined shell variable $(1) (used in $(project) $(2))<<)
endef 
define show_
$(info OK: $(1)=$($(1)))
endef

ifndef BASE_CH
$(info Error: shell variable BASE_CH is not defined)
$(info Read the makefile header info for details)
$(error )
else
$(call show_,BASE_CH)
endif

ifndef CONTROL_HOST
$(call wrn_,CONTROL_HOST,as default dispatcher host)
else 
$(call show_,CONTROL_HOST)
endif
#----------------------

CC=gcc

project = rc

CFLAGS += -Wall -O -g -std=c99 -I.  -I$(BASE_CH)/include
LDLIBS += -lm -lpthread -L$(BASE_CH)/lib -lconthost  

all : $(project) 

clean :
	rm -f *.o *~ $(project)

help:
	@echo 'make            build most recent source' 
	@echo 'make -Wrc.c     force re-building rc'                     
	@echo 'make clean      remove all binaries and garbage'                             
	@echo 'make help       show this help'                                  

.PHONY: all clean help 

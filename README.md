## Run Control for SHiP TB (draft version)
 
Run Control (draft): can be used to control running demo3 and demo4 clients

    Usage: 
    	  `./rc run# [-h DispHost{local}]
          [-n #of spills]
          [-c cycle period,s{40.0}]
          [-s spill length,s{5.0}]
          [-r recording{no recording}]
          [-t force to generate given av. # of triggers/spill]`

   (-c, -s and -t parameters are effective only with demo3)


    Example: ./rc 287 -n100 -c25 -s3 -r1 -t10000 -hshipdaq01

	   
    Manual controls (character entered at prompts or during the run:
            s - start run; t - stop run;"
	    n - next run; a  - stop program


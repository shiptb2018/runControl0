#define _BSD_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include "dispatch.h"
#include <errno.h>
#include <ctype.h>
#include <getopt.h>
extern char *optarg;                  // from getopt
extern int optind, opterr, optopt;    // from getopt

typedef struct { 
  uint16_t size;
  uint16_t partitionID;
  uint32_t cycleID;
  uint32_t frameTime;
  uint16_t timeExtent;
  uint16_t flags; 
} DataFrameHeader;

typedef struct {
  uint16_t channelId;
  uint16_t hitTime;
#ifdef EXTRADATA
  uint16_t extraData;
#endif
} RawDataHit;

typedef struct { 
  DataFrameHeader header;   
  RawDataHit hits[]; 
} DataFrame;
#define _ESC  "\x1b"   // escape char = 27
#define _R    _ESC"[31m" // red
#define _G    _ESC"[32m" // green
#define _Y    _ESC"[33m" // yellow
#define _Bu   _ESC"[34m" // blue
#define _M    _ESC"[35m" // magenta
#define _C    _ESC"[36m" // cyan
#define _D    _ESC"[0m"  // default (black)
#define _U    _ESC"[4m"  // underline
#define _B    _ESC"[1m"  // bold
#define _I    _ESC"[3m"  // italic

#define M_VERSION "0.2 07.06.2018"

#define SPECIAL_FRAME 0xFF00
#define SPECIAL_SOS   0x5C03
#define SPECIAL_EOS   0x5C04

#define BUF_SIZE 500
char buf[BUF_SIZE];
char *p_buf=buf;

FILE *outfile=NULL;

#define MX_CLIENTS 10
#define MX_NAME    100
#define MX_ERROR   10 
#define DAQCMD  "DAQCMD"
#define DAQACK  "DAQACK"
#define DAQDONE "DAQDONE"

enum Cntx {configured, running, paused, stopped, idle};

typedef struct {
  char nick[MX_NAME];
  char host[MX_NAME];
  uint16_t partID;
  char cpartID[5];
  char det[8];
  uint32_t Ndaqack;
  uint32_t Ndaqdone;
  uint32_t Ndaqdone_err[MX_ERROR];
  uint32_t Nframes;
  uint32_t Nfr_cycle;
  bool active;
  bool recording;
} Client_t;

typedef struct {
  uint32_t nspills;
  double cperiod; 
  double slen;
  double SoRdly;
  int TRGwait;
  bool rec;
} Conf_t;

typedef struct {
  char *host;
  int locdaqs;
  Client_t client[MX_CLIENTS];
  int runN;
  int runContext;
  Conf_t config;
  uint32_t cycleID;
  uint16_t cycle;
  bool inspill;
} RC_t;  

RC_t RC = {
  .host=NULL, .locdaqs=0, .runN=0, .runContext=idle, 
  .config = {.nspills=0, 
	     .cperiod=40.2, // s, cycle period 
	     .slen =5.1,    // s, spill length
	     .SoRdly=5.,    // s, max delay after SoR
	     .TRGwait=5,    // usleep(10) after every TRGwait loops
	     .rec=false},   // recording, by default
  .cycleID=0, .cycle=0
  };


int rc_findClients(char *host, const char* select, Client_t *client); 
int rc_cmdChk(const char *Cmdtg, const char *Cmd,  RC_t *RC, double Timeout, int expected);
void rc_prtHeader (void);
uint32_t getCycleId(int mode) ;
int rc_pclient(uint16_t partID);
int rc_iclient(const char *line);
void rc_abort(void);

char * chk(char *message);

int main(int argc, char *argv[]) {
  char tg[TAGSIZE+1];
  char list[200], cmd;
  char DataTags[100] =
    {"a RAW_0800 a RAW_0801 a RAW_0802 a RAW_0C00 a RAW_0B00 a RAW_0900"}; 

  DataFrame *fr;
  int i, rc, rw, sz, Ntriggers=5000;
  uint32_t cyc;
  time_t t1, t2;

  
  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  // Dispatcher host name
  if(argc == 1) {
    printf("\nUsage: %s run# [-h hostname{local}]\n"
	   "       [-n #of spills{manual control}]\n"
	   "       [-c cycle period,s{40.0}]\n"
	   "       [-s spill length,s{5.0}]\n"
	   "       [-r recording{no recording}]\n"
	   "       [-t force to generate given av. # of triggers/spill]\n"
	   "       [-d various delays (eg, -dr5.5: 5.5s max delay after SoR) \n"

	   "Manual control: s - start run; t - stop run;"
	   " n - next run; a  - stop program\n",argv[0]);
    return 0;
    } else {
    int opt;
    RC.runN = atoi(argv[1]); 
 
    while ((opt = getopt(argc, argv, "h:n:c:s:rt:d:?")) != -1) {
      switch (opt) {
      case 'h':  RC.host=optarg;                             break;
      case 'n':  RC.config.nspills = atoi(optarg);           break;
      case 'c':  RC.config.cperiod = atof(optarg);           break;
      case 's':  RC.config.slen = atof(optarg);              break;
      case 'r':  RC.config.rec= true;                        break;
      case 't':  Ntriggers = atoi(optarg);                   break;
      case 'd':  
	switch (optarg[0]) {
	case 'r': RC.config.SoRdly = atof(optarg+1);  break;
        case 'w': RC.config.TRGwait = atoi(optarg+1); break;
	}                                                    break;
      default :  fprintf(stderr,"invalid option %c\n",opt);  break;
      }
      assert(errno != ERANGE);
    }
  }

  if(RC.host==NULL) strcpy(list, "local host");
  else strncpy (list, RC.host, sizeof(list)-1);
  rc=init_2disp_link(RC.host,DataTags,"a STOP a DAQACK a DAQDONE"); // a DSENT 
  if( rc < 0 ) {
    fprintf(stderr," failed to connect to %s\n",list);
    exit(1); 
  }

  send_me_always();



  i=rc_findClients(RC.host,"_LocDaq_",RC.client);
  printf("RunConreol (draft) v.%s Data Tags: %s\n"
	 "Successfully connected to %s. %d clients are connected:\n",M_VERSION,DataTags,list,i);
  if(i==0) {
    fprintf(stderr,"Error: No connected LocDaq clients");
    exit(2);
  } 
    
  RC.locdaqs = i;

  rc = 0;
  for(i=0; i<RC.locdaqs; i++) {
    int j;
    printf("-- %02d %s\tpart %s\tid=%04X\t @ %s\n", 
	   i, RC.client[i].nick,  RC.client[i].det, RC.client[i].partID, RC.client[i].host);

    /* reset client statistics */
    RC.client[i].Ndaqack=0;
    RC.client[i].Ndaqdone=0;
    RC.client[i].Nframes=0;
    RC.client[i].Nfr_cycle=0;
    RC.client[i].active = false;

    memset(RC.client[i].Ndaqdone_err,0,sizeof(RC.client[i].Ndaqdone_err));

    if(i>0) {
      for(j=0; j<i; j++) {
	if(strcmp(RC.client[j].nick, RC.client[i].nick) == 0) {
	  fprintf(stderr, "Error: same partition %s is running on %s and %s\n",
		  RC.client[i].nick, RC.client[i].host, RC.client[j].host);
	  rc--;
	}
      }
    }
  }
  if(rc < 0) exit(3);
  
  /* check that all connected clients are alive */
  rc=rc_cmdChk(DAQCMD,"Enable test",&RC,0.1,-1);  // exit on timout 100 ms
  if(rc != RC.locdaqs*2) {
    fprintf(stderr, "Error: missing/spurious responses from connected partitions:"
	    " %d responses received/%d expected\n",rc,RC.locdaqs*2);
    exit(1);
  }
  
  RC.runContext = configured;
  RC.inspill = false;

  rc_prtHeader();

 RunReady: 
  if(RC.runContext == stopped)
    RC.runN ++;

  while(1) {
    scanf("%s%*c",list);
    if(strchr("nsa",list[0]) !=NULL) {
      cmd=list[0];
      break;
    }
  }

  if(cmd=='a') rc_abort();  

  sprintf(list,"SoR %d",RC.runN);
  //rc = put_fullstring(DAQCMD,list);
  rc=rc_cmdChk(DAQCMD,list,&RC,RC.config.SoRdly, RC.locdaqs*3); // expect ACK, DONE and SoR frame 
  if(rc != RC.locdaqs*3) {
    fprintf(stderr, "Error: missing/spurious responses to SoR:"
	    " %d responses received/%d expected\n",rc,RC.locdaqs*3);
    exit(1);
  }

  RC.runContext = running;
  
  unsigned long loops=0;
  for(cyc=0; cyc<RC.config.nspills; cyc++){
    double dT;
    int nnmax; // longest usleep between check_head's
    t1=t2=time(NULL);
    printf("-------------------------------------------------- cyc=%d\n",cyc);
    RC.cycleID=getCycleId(0) ;
    RC.cycle = cyc;
    sprintf(list,"SoS %X %d",RC.cycleID,Ntriggers);   
    rc = put_fullstring(DAQCMD,list);
    RC.inspill = true;
    for(i=0; i<RC.locdaqs; i++)  RC.client[i].Nfr_cycle=0;
    rc_prtHeader();

    cmd=0; 
    nnmax=0;
    while ((dT=difftime(time(NULL),t1)) < RC.config.cperiod) {
      int initsleep=10, nn=initsleep;
      loops++;
      if(((loops%=10000) == 0) && (chk(list) != NULL)) {
	cmd = list[0]; // pick up the entered character
      }
      
      if((loops%RC.config.TRGwait) == 0 ) {
	usleep(10);  // interrupt after every 10000 loops, to give way to EVB
      }
      
      rw=check_head(tg,&sz);
      if(rw > 0) {
	int iclient, pclient;

	nn=initsleep;

	if(strstr("DAQCMD DAQACK DAQDONE",tg) != NULL) {
	  if((rc = get_string(p_buf,sz)) < 0) {
	    fprintf(stderr, "Err: rc=get_string=%d. tag=%s sz=%d\n",rc,tg,sz);
	    exit(3);
	  }
//	  printf("----- cmd tag=%s sz=%d \t %s\n",tg,sz,p_buf); 
	  if( (iclient = rc_iclient(p_buf)) >=0 ) {
	    if( tg[3]=='A') RC.client[iclient].Ndaqack++; // DAQACK
	    else {
	      RC.client[iclient].Ndaqdone++;// DAQDONE
	    }
	  }
	} 
	else {
	  if((rc = get_data(p_buf,16)) < 0) {
	    printf("Err: rc=get_data=%d. tag=%s sz=%d\n",rc,tg,sz);
	    exit(3);
	  }
	  fr = (DataFrame *) p_buf;
	  if( (pclient = rc_pclient(fr->header.partitionID)) >=0 ) {
	      RC.client[pclient].Nframes++;// total # of frames
	      RC.client[pclient].Nfr_cycle++;// # of frames per cycle
/*	      
      printf("      data tag=%s sz=%d \t part %04X cyc %X time %X/%d ext %d fl %x \n",
	     tg,sz, 
	     fr->header.partitionID,
	     fr->header.cycleID,
	     fr->header.frameTime, fr->header.frameTime,
	     fr->header.timeExtent,
	     fr->header.flags);
*/	     
	  }
	}
      }
      else if (rw == 0 ) {
	nn++;
	if(nn>nnmax) nnmax=nn;
	usleep(nn);
      }

      else {
	fprintf(stderr,"Error: unexpected check_head rc=%d\n",rw);
	exit(1);
      }

      if ( (dT > RC.config.slen) && RC.inspill) {
        rc = put_fullstring(DAQCMD,"EoS");
	RC.inspill = false;
	//	rc_prtHeader();
      }

      if (difftime(time(NULL),t2) > 2. ) {
	//      printf("----------- inspill %d dT %5.1f\n",RC.inspill,dT);
	t2 = time(NULL);
	rc_prtHeader();
      }
    }
      /* respond to entered character, if any */
    if(cmd != 0) {
      switch (cmd) {
      case 't' : 
	goto RunEnd;
      case 'p' : {
	RC.runContext = paused;
	rc_prtHeader();
	while(1) {
	  scanf("%s%*c",list); 
	  cmd=list[0];
	  if(cmd=='t') goto RunEnd;
	  if(cmd=='r') {
	    cmd=0;
	    RC.runContext = running;
	    break;
	  }
	} // wait for response to "pause"
      } // "pause"
      } // parse keyboard input
    } // check for keyboard input
    //printf("Longest usleep nnmax=%d\n",nnmax);
  } // loop over cycles
 RunEnd:
  rc = put_fullstring(DAQCMD,"EoR");
  RC.runContext = stopped;
  rc_prtHeader();
  goto RunReady;
}


int rc_findClients(char *host, const char* select, Client_t *client) {

  FILE *ls;
  char *comma, *dot,*und,  buf[256], nick[100]={"dispstat"}, node[100];
  int locdaqs=0,i=0;
  if(host!=NULL) sprintf(nick,"dispstat %s",host);
  //printf("cmd=|%s|\n",nick);
  ls = popen(nick, "r");
  while (fgets(buf, sizeof(buf), ls) != 0) {
    if(strstr(buf,"nick")!=NULL) {
      sscanf(buf, "%s %s %s %s %s %s", nick, nick, nick, node, nick,nick);
      comma=strrchr(nick,','); 
      if(comma !=NULL) *comma='\0';
      comma=strrchr(node,','); 
      if(comma !=NULL) *comma='\0';
      if(strstr(nick,select)) locdaqs++;
      //printf("%d(%d) |%s| on |%s|\n",i,locdaqs,nick,node);
      if((dot=strchr(node,'.')) != NULL) *dot='\0';
      strncpy(client[i].nick, nick,MX_NAME-1);
      strncpy(client[i].host, node,MX_NAME-1);
      und=strrchr(nick,'_');
      client[i].partID = strtol(und+1,NULL,16);
      strncpy(client[i].cpartID,und+1,4);
      und=strchr(nick,'_');
      *und='\0';
      strcpy(client[i].det, nick);
      i++;
      if(i == MX_CLIENTS) break;
    }
  }
  pclose(ls);
  return locdaqs;
}

int rc_cmdChk(const char *Cmdtg, const char *Cmd,  RC_t *RC, double Timeout, int nExpectedResponses) {
  char tg[TAGSIZE+1];
  int rc;
  int i,sz,nn=10;
  char cmdname[100],line[100], *c, Nreplies=0;
  DataFrame *fr;
  struct timeval t1,t2;
  double elapsed_s=0.;
  
  gettimeofday(&t1,NULL);
  rc = put_fullstring(Cmdtg,Cmd);              // first of all, send the command
  strncpy(cmdname,Cmd,sizeof(cmdname)-1);
  if((c=strchr(cmdname,' ')) != NULL) *c='\0'; // extract the Command name
  
  /* now, monitor all replies */
  while(1) {
    while ((rc=check_head(tg,&sz)) <= 0) {
      /* increment usleep by 10 us at each iteration */
      //      nn++;
      nn += 10;
      usleep(random()%nn); 
      //if(nn%10 == 0) printf("nn=%d\n",nn);
      gettimeofday(&t2,NULL);
      elapsed_s = (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec)*1.e-6; // s 
      if(elapsed_s > Timeout) { 
	/* Timeout detected */
	printf(">>>>>>>>> Timeout after %6.3f s. Nreplies: %d\n",elapsed_s,Nreplies);
	return Nreplies;
      }
    }
    Nreplies++;

    if(strstr("DAQACK DAQDONE",tg) != NULL) {
      get_string(line,sizeof(line));
      printf("received #%d tag %s response=%s, after %6.3f s:\n",
	   Nreplies,tg,line,elapsed_s);
      if(strstr(line,cmdname) != NULL) {
        i = rc_iclient(line);
        if(i>=0) {
	  RC->client[i].active = true;
	  if( tg[3]=='A') RC->client[i].Ndaqack++; // DAQACK
	  else {
	    RC->client[i].Ndaqdone++;// DAQDONE
	    sscanf(line,"%*s %*s %d",&rc);  // return code in DAQDONE response
	    if(rc != 0) {
	      if(rc<0 || rc>MX_ERROR) rc = 0;
	      RC->client[i].Ndaqdone_err[rc]++;//  abnormal rc in DAQDONE
	    }
	  }
	} // i>=0
      } // if response to this command Cmd
    } // if ACK or DONE
 
    else { // data
      if((rc = get_data(p_buf,16)) < 0) {
	printf("Err: rc=get_data=%d. tag=%s sz=%d\n",rc,tg,sz);
	exit(3);
      }

      fr = (DataFrame *) p_buf;
      printf("received #%d tag %s frame time=%08x, after %6.0f ms:\n",
	   Nreplies,tg,fr->header.frameTime,elapsed_s);
      if( (i = rc_pclient(fr->header.partitionID)) >=0 ) {
	RC->client[i].Nframes++;// total # of frames
	RC->client[i].Nfr_cycle++;// # of frames per cycle
      }
    }  // if data

    /* Exit if the expected # of responses is reached */
    if(nExpectedResponses>0 && Nreplies == nExpectedResponses) return Nreplies;

  }
}
	
  void rc_abort(void) {
    printf("Run Control program is aborted\n");
    exit(0);
  }

 int rc_iclient(const char *line) {
   /* recognize the client by the partID as char string */
   int i;
   for(i=0; i<(RC.locdaqs); i++) {
     if(strstr(line,RC.client[i].cpartID) != NULL) return i;
   }
   return -1;
 }

 int rc_pclient(uint16_t partID) {
   /* recognize the client by the partID as integer */
   int i;
   for(i=0; i<(RC.locdaqs); i++) {
     if(RC.client[i].partID == partID) return i;
   }
   return -1;
 }


void rc_prtHeader(void) {
  int i,l;
  char *c;
  static const char *rCntx[]=
    {_C"CONFIGURED"_D, _G"RUNNING"_D, _Y"PAUSED"_D,_R"STOPPED"_D, "IDLE"}; 
  static const char *commands[]= {
    "Start Change Abort",              // configured
    "sTop Pause",                      // running
    "sTop Resume Change Manual_cycle", // paused
    "New_run Abort",                   // stopped
    "New_run Abort" 
  };
  /*
  printf("Run %d %s host=%s nspills=%d cperiod=%4.1f s slen=%4.1f s rec=%c\n",
	 RC.runN, rCntx[RC.runContext],
	 RC.host, 
	 RC.config.nspills, RC.config.cperiod, RC.config.slen, RC.config.rec?'t' : 'f');
  */
  printf("Run %d %s cyc %d/%X inspill=%c",RC.runN,rCntx[RC.runContext],RC.cycle,RC.cycleID,RC.inspill?'t':'f' );
  for(i=0; i<RC.locdaqs; i++) {
    printf("\t%s%s%s /%d/%d/%d/%d",
	   RC.client[i].active?_G:_R, RC.client[i].det,_D, 
	   RC.client[i].Ndaqack, RC.client[i].Ndaqdone,
	   RC.client[i].Nframes, RC.client[i].Nfr_cycle);
  }
  printf("\nRC commands: ");
  c=(char *)commands[RC.runContext];
  l=strlen(c);
  for(i=0; i<l; i++) {
    if(islower(*c)) fputc(*c,stdout);
    else printf("%s%c%s",_B,tolower(*c),_D);
    c++;
  }
  printf("\n");
}

uint32_t getCycleId(int mode) {
  /*
   * Cycle ID generator, according to MJ+PG "Raw data format" memo
   * mode: =0, use OS clock; =1, (not yet implemented) use external id from the central DAQ message 
   * Author: PG, Jan 2018
   */

  static time_t time_offset=-1, time_now;
  if((long int) time_offset==-1) {
    char s[200];
    struct tm tp={0,0,12,8,3,115,0,0,0}; // 8th of April 2015, 12:00 am
    strftime(s,199,"Time offset date: %x time %X",&tp);
    //printf("%s\n",s);
    time_offset=mktime(&tp);
  }
  time_now=time(NULL); 
  //printf("current time=%ld  time0=%ld diff %5.1f years\n",time_now, time_offset,
  //	 difftime(time_now,time_offset)/3.154E7);
  return difftime(time_now,time_offset)*5;
}


/*  prepartion for future  
struct timeval t1,t2;

    char message[50];
    double elapsedTime;

    setbuf(stdout,NULL);
    chk(NULL);

    while(1)
    {
      gettimeofday(&t1,NULL);
      while (1) {
	usleep(100); gettimeofday(&t2,NULL);
	elapsedTime = (t2.tv_sec - t1.tv_sec);      // sec to ms
	elapsedTime += (t2.tv_usec - t1.tv_usec) * 1.e-6;   // us to ms
	if(elapsedTime>atof(argv[1])) break;
      }
      if(chk(message) == NULL) {
	printf(".");
	//          nanosleep(&t_dly , &t_dum);
      } else {
	printf("\nMessage: %s\n", message);
      }
    }
*/

/*
 * non-blocking character input
 */

char * chk(char *message) {
  static     fd_set readfds;
  static     struct timeval timeout;
  if(message == NULL) {
    FD_ZERO(&readfds);
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;
    return NULL;
  } else { 
    FD_SET(STDIN_FILENO, &readfds);
    
    if (select(1, &readfds, NULL, NULL, &timeout))
      {
	scanf("%s", message); 
	return message;
      }
    return NULL;
  }
}


